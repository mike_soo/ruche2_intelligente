clear all; close all; clc;
xaxis = [ 2 4 6 8 10 12 14 16 18 20 ];

nanoAmp = [2 3.8 4.5 5.3 6.5 7.6 8.7 9.5 10.9 11.8];
stmAmp = [];

for i= 1:length(xaxis)
    stmAmp = [stmAmp ampStm32L4cons(xaxis(i))];
end

figure('name' , 'Comparaison de consomation de courrant');
title('Consommation de courant stm32L4 et arduino nano.');

xlabel('fréquence CPU [MHz] ');
ylabel('courant [mA]');
hold on;
plot(xaxis , nanoAmp , 'o-');
plot(xaxis , stmAmp  , 'o-');
legend('Arduino nano', 'Stm32L432K');
hold off;