% return the current consumption in function of 
%the current frequency in mA.
function a = ampStm32L4cons(f)
    a = 107 * f / 1000;
end