#!/usr/bin/python3


import serial

ser = serial.Serial('/dev/ttyACM0', 9600, timeout=2, xonxoff=False, rtscts=False, dsrdtr=False) #Tried with and without the last 3 parameters, and also at 1Mbps, same happens.
ser.flushInput()
ser.flushOutput()
print("start")
f=open("log.txt", "a+")
while True:
  try:
    data_raw = ser.readline()
    print(str(data_raw))
    f.write(str(data_raw) + "\n")
  
  except KeyboardInterrupt:
        print ("[CTRL+C detected]")
        f.close()
        break

print("Bye")
  
