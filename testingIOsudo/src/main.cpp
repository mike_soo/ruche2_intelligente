#include <stdio.h>
#include "mbed.h"
#include "BMP180.h"

I2C i2c(D14, D15);
BMP180 bmp180(&i2c);

int main(void) {

    while(1) {
        if (bmp180.init() != 0) {
            printf("Error communicating with BMP180\r\n");
        } else {
            printf("Initialized BMP180\r\n");
            break;
        }
        wait(1);
    }

    while(1) {
        bmp180.startTemperature();
        wait_ms(5);     // Wait for conversion to complete
        float temp;
        if(bmp180.getTemperature(&temp) != 0) {
            printf("Error getting temperature\r\n");
            continue;
        }

        bmp180.startPressure(BMP180::ULTRA_LOW_POWER);
        wait_ms(10);    // Wait for conversion to complete
        int pressure;
        if(bmp180.getPressure(&pressure) != 0) {
            printf("Error getting pressure\r\n");
            continue;
        }

        printf("Pressure = %d Pa Temperature = %f C\r\n", pressure, temp);
        wait(1);
    }
}