function filteredData = filterCardData( chipData , maxValue )

	decalage =  -chipData(1 , 1);
	chipData = [(chipData(: , 1) + decalage) chipData(: , 3) * 1000];
	;

	chipData(chipData(: , 2) >  maxValue == 1 , :) = [];

	chipData(chipData(: , 1) <= 15   == 0 , :) = [];

	filteredData = chipData;
	

end