clear all; close all ; clc;

run data_card_with_comps.m
run data_card_no_comps.m

dataCardNoComps   = filterCardData(dataCardNoComps , 55);
dataCardWithComps = filterCardData(dataCardWithComps , 80);

figure();
plot (dataCardWithComps(: , 1) , dataCardWithComps(:,2));
title('Consommation de courant: stm32L432K + tous les composants.');

xlabel('temps [s]');
ylabel('courant [mA]');
axis([0 15 0 90]);

figure();
plot (dataCardNoComps(: , 1) , dataCardNoComps(:,2));
title('Consommation de courant: stm32L432K sans composants.');

xlabel('temps [s]');
ylabel('courant [mA]');

axis([0 15 0 90]);

