clear all ; clc ; close all;

% Correction de la calibration 
% passage de coef 11200 à 11900
run filtered_poids_measure_nuit_28_01_2019.m;
% masses = masses * 11200 / 11900 * (-1);

indexAdd = floor(size(filtered_values_dates, 1 ) / 8);

datesToShow=[];
for i=1:8
    datesToShow = [datesToShow ; filtered_values_dates(i * indexAdd , :)];

end

datesToShow

figure()
hold on
plot(masses );
set(gca, 'xticklabel',datesToShow)
xtickangle(45)
hold off
xlabel('Temps [hh:mm:ss]');
ylabel('masse [Kg]');
title('Variation de la masse au cours du temps ')


figure()
hold on
plot(temperatures );
set(gca, 'xticklabel',datesToShow)
xtickangle(45)
xlabel('Temps hh:mm:ss');
ylabel('Température [C°]');
title('Variation de la température au cours du temps ')
hold off


figure()
hold on 
plot (temperatures , masses);
xlabel('Température [C°]');
ylabel('Poids [kg]')
title('Variation du poids en fonction de la température')
hold off


['Corection des poids mesurés']
alpha = -0.15;
temperRef = temperatures(1)
poidsCors = []
for i=2:size(masses , 1)
    temperCour = temperatures(i  ,1);
    delta =  temperRef - temperCour;
    poidsMes = masses(i , 1)
    poidsCor = poidsMes + (alpha * delta)    
    poidsCors = [poidsCors; poidsCor]
end

poidsRef = masses(1, 1)
errPoidsNonCors = (masses(:,1) - poidsRef)
errPoidsCors   = (poidsCors(:,1) - poidsRef )


figure()
hold on
plot(errPoidsCors );
plot(errPoidsNonCors );
set(gca, 'xticklabel',datesToShow)
xtickangle(45)
xlabel('Temps hh:mm:ss');
ylabel('Erreur poids [kg]');
title('Erreurs de mesures avec et sans méthode de correction. ')
legend('Correction active','Correction non active')
hold off
