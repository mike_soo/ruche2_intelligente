#!/usr/bin/python3
import pprint
id = 0
measurements = {}

with open("poids_measure_nuit_26-01-2019.txt") as f:
    
    for line in f:
        splitedLine = line.split(" ")
        
        
        try:
            measurements[id] = {}
           

            for i in range(4):
                dicoel = splitedLine[i].split(":")
                measurements[id][dicoel[0]] = float(dicoel[1])
                measurements[id]["time"]  = str(splitedLine[len(splitedLine) -1].replace("\n",""))
                measurements[id]["date"] = str(splitedLine[len(splitedLine) -2])

            
            id = id + 1
        except Exception as e :
            print (e)
            continue

f = open("filtered_poids_measure_nuit_26_01_2019.m","w+")
f.write("filtered_values_dates = [")
for key , value in measurements.items():
    try:
        if str(value["time"]) > "01:54:00.000000":
            break
        f.write("'" + str(value["time"]) + "';\n" )
    except Exception as e:
        print(e)
        continue
f.write("];\n")


f.write("filtered_values = [")
for key , value in measurements.items():
    try:
        if str(value["time"]) > "01:54:00.000000":
            break
        print(value)
        f.write(str(value["te"]) + " , " + str(value["m"])   + ";\n" )
    except Exception as e:
        print(e)
        continue
f.write("];\n")

f.close()